import os
import cv2 as cv
import json
import numpy as np
from scipy import ndimage as ndi
from skimage.exposure import rescale_intensity
from skimage.segmentation import watershed
import colorcorrect.algorithm as cco
from PIL import Image
from flask import Flask, flash, request, redirect, send_file, url_for

from werkzeug.utils import secure_filename

UPLOAD_FOLDER = f'{os.getcwd()}/uploads'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'tiff'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
  return '.' in filename and \
    filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    return 'Hello, World!'

@app.route('/upload', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            #file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            img = Image.open(file)
            img = np.array(img)

            send_picture = request.args.get('image') == '1'

            data = segment_cells(img, filename, send_picture)

            if send_picture:
              return send_file(os.path.join(app.config['UPLOAD_FOLDER'], f"analysed_{filename}"), mimetype='image/gif')
            else:
              return data

def fill_holes(img_th):
    im_floodfill = img_th.copy()
    h, w = img_th.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)
    cv.floodFill(im_floodfill, mask, (0,0), 255);
    im_floodfill_inv = cv.bitwise_not(im_floodfill)
    im_out = img_th | im_floodfill_inv
    return im_out

def white_balance(img):
    result = cv.cvtColor(img, cv.COLOR_RGB2LAB)
    avg_a = np.average(result[:, :, 1])
    avg_b = np.average(result[:, :, 2])
    result[:, :, 1] = result[:, :, 1] - ((avg_a - 128) * (result[:, :, 0] / 255.0) * 1.1)
    result[:, :, 2] = result[:, :, 2] - ((avg_b - 128) * (result[:, :, 0] / 255.0) * 1.1)
    result = cv.cvtColor(result, cv.COLOR_LAB2RGB)
    return result

def brigten_mask_area(img, pixel_range = (0,255), boost_by = 10):
  x_size, y_size = img.shape

  img_return = img.copy()

  for i in range(x_size):
    for j in range(y_size):
      pixel_value = img[i,j]

      if (pixel_value >= pixel_range[0]) and (pixel_value <= pixel_range[1]):
        if pixel_value + boost_by >= 0:
          if pixel_value + boost_by <= 255:
            img_return[i,j] = pixel_value + boost_by
          else:
            img_return[i,j] = 255
        else:
          img_return[i,j] = 0

  return img_return

def segment_cells(img, filename, send_picture: bool = False):
  images = [img]
  images_converted = [cv.cvtColor(i, cv.COLOR_BGR2RGB) for i in images]
  images_denoised = [cv.fastNlMeansDenoisingColored(i, None, 7, 7, 51, 10) for i in images]
  images_lwgw = [cco.luminance_weighted_gray_world(i) for i in images_denoised]
  images_balanced = [white_balance(i) for i in images_lwgw]
  lower_index = [False] * len(images_balanced)
  bins_interval = np.linspace(0,256,8)

  for i in range(len(images_balanced)):
    b, g, r = cv.split(images_balanced[i])
    b_hist, _ = np.histogram(b, bins = bins_interval)
    g_hist, _ = np.histogram(g, bins = bins_interval)
    r_hist, _ = np.histogram(r, bins = bins_interval)
    
    lower_index[i] = not (np.sum(b_hist[0:3]) + np.sum(g_hist[0:3]) >= np.sum(r_hist[0:3]))

  images_hsv_v = []

  for i in images_balanced:
    img_hsv = cv.cvtColor(i, cv.COLOR_BGR2HSV)
    _, _, v = cv.split(img_hsv)
    images_hsv_v.append(v)

  images_v_inv = []

  for i in images_hsv_v:
    v_inv = cv.bitwise_not(i)
    v_denoised = v.copy()
    cv.fastNlMeansDenoising(v_inv, v_denoised, 7, 7, 51)
    images_v_inv.append(v_denoised)

  images_v_rescaled = []

  for i in range(len(images_v_inv)):
    if lower_index[i]:
      images_v_rescaled = [rescale_intensity(i, in_range=(80, 130)) for i in images_v_inv]
    else:
      images_v_rescaled = [rescale_intensity(i, in_range=(70, 150)) for i in images_v_inv]

  images_v_threshold = []
  for i in range(len(images_v_rescaled)):
    if lower_index[i]:
      _, thr = cv.threshold(images_v_rescaled[i],180,255, cv.THRESH_BINARY)
      images_v_threshold.append(thr)
    else:
      _, thr = cv.threshold(images_v_rescaled[i],220,255, cv.THRESH_BINARY)
      images_v_threshold.append(thr)

  images_v_rescaled_background = []
  for i in range(len(images_v_inv)):
    if lower_index[i]:
      v_rescaled_background = rescale_intensity(images_v_inv[i], in_range=(55, 255))
      v_bright_edit = brigten_mask_area(v_rescaled_background, pixel_range=(48,255), boost_by=100)
      images_v_rescaled_background.append(v_bright_edit)
    else:
      v_rescaled_background = rescale_intensity(images_v_inv[i], in_range=(70, 255))
      v_bright_edit = brigten_mask_area(v_rescaled_background, pixel_range=(50,255), boost_by=100)
      images_v_rescaled_background.append(v_bright_edit)

  images_v_threshold_background = []
  for i in images_v_rescaled_background:
    _, thr = cv.threshold(i,150,255, cv.THRESH_BINARY)
    images_v_threshold_background.append(thr)

  kernel_mask = np.ones((3,3),np.uint8)

  n = 10
  over_iterate = 3

  images_v_morphed = []

  for i in images_v_threshold:
    v_thr_copy = i.copy()
    for i in range(n):
      v_eroded = cv.morphologyEx(v_thr_copy, cv.MORPH_ERODE,kernel_mask, iterations=over_iterate)
      v_thr_copy = cv.morphologyEx(v_eroded, cv.MORPH_DILATE,kernel_mask, iterations=over_iterate)
    images_v_morphed.append(v_thr_copy)

  images_v_filled = [fill_holes(i) for i in images_v_morphed]

  images_v_cleaned = []
  min_size = 100 

  for i in images_v_filled:
    nb_blobs, im_with_separated_blobs, stats, _ = cv.connectedComponentsWithStats(i)
    sizes = stats[:, -1]
    sizes = sizes[1:]
    nb_blobs -= 1
    img_cleaned = np.zeros((i.shape))
    for blob in range(nb_blobs):
        if sizes[blob] >= min_size:
            img_cleaned[im_with_separated_blobs == blob + 1] = 255
    images_v_cleaned.append(img_cleaned)

  v_cleaned = images_v_cleaned[0]

  # noise removal
  kernel = np.ones((3,3),np.uint8)
  opening = cv.morphologyEx(v_cleaned,cv.MORPH_OPEN,kernel, iterations = 2)
  opening = np.uint8(opening)

  dist_transform = cv.distanceTransform(opening,cv.DIST_L2,5)
  ret, sure_fg = cv.threshold(dist_transform,0.15*dist_transform.max(),255,0)
  sure_fg = np.uint8(sure_fg)

  distance = ndi.distance_transform_edt(opening)

  _, markers = cv.connectedComponents(opening)
  markers = watershed(-distance, markers, mask=opening, watershed_line=False)

  v_threshold_background = images_v_threshold_background[0]

  # noise removal
  kernel = np.ones((3,3),np.uint8)
  opening_background = cv.morphologyEx(v_threshold_background,cv.MORPH_OPEN,kernel, iterations = 2)
  opening_background = np.uint8(opening_background)

  distance = ndi.distance_transform_edt(opening_background)

  _, markers_background = cv.connectedComponents(sure_fg)

  markers_background = watershed(-distance, markers_background, mask=opening_background, watershed_line=False)

  x_size, y_size = markers.shape

  markers_mask = np.zeros(markers.shape)

  for i in range(x_size):
    for j in range(y_size):
      if markers[i,j] >= 1:
        markers_mask[i,j] = 255

  markers_mask = np.array(markers_mask, np.uint8)

  num_unique = np.unique(markers_background)
  num_unique = np.delete(num_unique, 0)

  img_norm_contours = images_converted[0].copy()
  contours, _ = cv.findContours(markers_mask, cv.RETR_LIST, cv.CHAIN_APPROX_NONE)

  def filterMap(map, target: int):
    x, y = map.shape
    map_copy = map.copy()
    for i in range(x):
      for j in range(y):
        if (map[i,j] != target):
          map_copy[i,j]= 0
        else:
          map_copy[i,j] = 1
    return map_copy

  data_return = []
  counter = 0
  for contour in contours:
    area = cv.contourArea(contour)
    if area >= 200:
      cv.drawContours(img_norm_contours, contour, -1, (0, 255, 0), 2)
      
      M = cv.moments(contour)
      cX = int(M["m10"] / M["m00"])
      cY = int(M["m01"] / M["m00"])

      cv.circle(img_norm_contours, (cX, cY), 2, (255, 255, 255), -1)

      targetNumber = markers_background[cY, cX]
      filter_map = filterMap(markers_background, targetNumber)
      filter_map = np.uint8(filter_map)
      contours_background, _ = cv.findContours(filter_map, cv.RETR_LIST, cv.CHAIN_APPROX_NONE)
      shell_data = []
      for c in contours_background:
        area_container = cv.contourArea(c)
        if area_container >= 200:
          cv.drawContours(img_norm_contours, c, -1, (0, 0, 255), 2)
          shell_data.append({'container_area': area_container, 'container_points': c.tolist()})

      counter = counter + 1
      data_data = {'id': counter, 'center': {'x': cY, 'y': cX}, 'core_points': contour.tolist(),'core_area': area, 'shell': shell_data}
      data_return.append(data_data)
  
  data = {'filename': 'filename', 'shape': img_norm_contours.shape, 'data': data_return}
  data_json = json.dumps(data)

  cv.imwrite(os.path.join(app.config['UPLOAD_FOLDER'], f"analysed_{filename}"), img_norm_contours)

  return data_json

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5002, debug=True)
